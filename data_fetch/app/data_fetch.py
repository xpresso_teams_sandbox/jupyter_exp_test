MOUNT_PATH = None
import os
import sys
import pickle
import urllib
import sklearn
import numpy as np
import pandas as pd
import matplotlib as mpl
import sklearn.neighbors
import sklearn.linear_model
from sklearn import pipeline
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn import preprocessing
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

try:
    None = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/None.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = jupyter_exp_test
## $xprparam_componentname = data_fetch
## $xprparam_componenttype = pipeline_job
## $xprparam_global_variables = [None]
## $xprjobparam_learningrate = 0.4# Python ≥3.5 is required
assert sys.version_info >= (3, 5)# Scikit-Learn ≥0.20 is required
assert sklearn.__version__ >= "0.20"This function just merges the OECD's life satisfaction data and the IMF's GDP per capita data. It's a bit too long and boring and it's not specific to Machine Learning, which is why I left it out of the book.def prepare_country_stats(oecd_bli, gdp_per_capita):
    oecd_bli = oecd_bli[oecd_bli["INEQUALITY"]=="TOT"]
    oecd_bli = oecd_bli.pivot(index="Country", columns="Indicator", values="Value")
    gdp_per_capita.rename(columns={"2015": "GDP per capita"}, inplace=True)
    gdp_per_capita.set_index("Country", inplace=True)
    full_country_stats = pd.merge(left=oecd_bli, right=gdp_per_capita,
                                  left_index=True, right_index=True)
    full_country_stats.sort_values(by="GDP per capita", inplace=True)
    remove_indices = [0, 1, 6, 8, 33, 34, 35]
    keep_indices = list(set(range(36)) - set(remove_indices))
    return full_country_stats[["GDP per capita", 'Life satisfaction']].iloc[keep_indices]The code in the book expects the data files to be located in the current directory. I just tweaked it here to fetch the files in datasets/lifesat.datapath = os.path.join("datasets", "lifesat", "")# To plot pretty figures directly within Jupyter

mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)# Download the data
DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"
os.makedirs(datapath, exist_ok=True)
for filename in ("oecd_bli_2015.csv", "gdp_per_capita.csv"):
    print("Downloading", filename)
    url = DOWNLOAD_ROOT + "datasets/lifesat/" + filename
    urllib.request.urlretrieve(url, datapath + filename)# Code example

# Load the data
oecd_bli = pd.read_csv(datapath + "oecd_bli_2015.csv", thousands=',')
gdp_per_capita = pd.read_csv(datapath + "gdp_per_capita.csv",thousands=',',delimiter='\t',
                             encoding='latin1', na_values="n/a")

# Prepare the data
country_stats = prepare_country_stats(oecd_bli, gdp_per_capita)
X = np.c_[country_stats["GDP per capita"]]
y = np.c_[country_stats["Life satisfaction"]]

# Visualize the data
country_stats.plot(kind='scatter', x="GDP per capita", y='Life satisfaction')
xpresso_save_plot("image_1", output_path=MOUNT_PATH,output_folder="jupyter_exp_test/data_fetch")

# Select a linear model
model = sklearn.linear_model.LinearRegression()

# Train the model
model.fit(X, y)

# Make a prediction for Cyprus
X_new = [[22587]]  # Cyprus' GDP per capita
print(model.predict(X_new)) # outputs [[ 5.96242338]]

try:
    pickle.dump(None, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/None.pkl", "wb"))
except Exception as e:
    print(str(e))
