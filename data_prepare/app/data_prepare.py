MOUNT_PATH = None
import os
import sys
import pickle
import urllib
import sklearn
import numpy as np
import pandas as pd
import matplotlib as mpl
import sklearn.neighbors
import sklearn.linear_model
from sklearn import pipeline
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn import preprocessing
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

try:
    oecd_bli = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/oecd_bli.pkl", "rb" ))
    gdp_per_capita = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/gdp_per_capita.pkl", "rb" ))
    X = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/X.pkl", "rb" ))
    Y = pickle.load(open( "/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/Y.pkl", "rb" ))
except Exception as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xprparam_projectname = jupyter_exp_test
## $xprparam_componentname = data_prepare
## $xprparam_componenttype = pipeline_job
## $xprparam_global_variables = ["oecd_bli", "gdp_per_capita", "X", "Y"]
## $xprjobparam_learningrate = 0.4# Note: you can ignore the rest of this notebook, it just generates many of the figures in chapter 1.Create a function to save the figures.# Where to save the figures
PROJECT_ROOT_DIR = "."
CHAPTER_ID = "fundamentals"
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID)
os.makedirs(IMAGES_PATH, exist_ok=True)

def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)Make this notebook's output stable across runs:np.random.seed(42)# Load and prepare Life satisfaction dataIf you want, you can get fresh data from the OECD's website.
Download the CSV from http://stats.oecd.org/index.aspx?DataSetCode=BLI
and save it to `datasets/lifesat/`.oecd_bli = pd.read_csv(datapath + "oecd_bli_2015.csv", thousands=',')
#oecd_bli = oecd_bli[oecd_bli["INEQUALITY"]=="TOT"]
#oecd_bli = oecd_bli.pivot(index="Country", columns="Indicator", values="Value")
oecd_bli.head(2)oecd_bli["Life satisfaction"].head()# Load and prepare GDP per capita dataJust like above, you can update the GDP per capita data if you want. Just download data from http://goo.gl/j1MSKe (=> imf.org) and save it to `datasets/lifesat/`.gdp_per_capita = pd.read_csv(datapath+"gdp_per_capita.csv", thousands=',', delimiter='\t',
                             encoding='latin1', na_values="n/a")
gdp_per_capita.rename(columns={"2015": "GDP per capita"}, inplace=True)
gdp_per_capita.set_index("Country", inplace=True)
gdp_per_capita.head(2)full_country_stats = pd.merge(left=oecd_bli, right=gdp_per_capita, left_index=True, right_index=True)
full_country_stats.sort_values(by="GDP per capita", inplace=True)
full_country_statsfull_country_stats[["GDP per capita", 'Life satisfaction']].loc["United States"]remove_indices = [0, 1, 6, 8, 33, 34, 35]
keep_indices = list(set(range(36)) - set(remove_indices))

sample_data = full_country_stats[["GDP per capita", 'Life satisfaction']].iloc[keep_indices]
missing_data = full_country_stats[["GDP per capita", 'Life satisfaction']].iloc[remove_indices]sample_data.plot(kind='scatter', x="GDP per capita", y='Life satisfaction', figsize=(5,3))
plt.axis([0, 60000, 0, 10])
position_text = {
    "Hungary": (5000, 1),
    "Korea": (18000, 1.7),
    "France": (29000, 2.4),
    "Australia": (40000, 3.0),
    "United States": (52000, 3.8),
}
for country, pos_text in position_text.items():
    pos_data_x, pos_data_y = sample_data.loc[country]
    country = "U.S." if country == "United States" else country
    plt.annotate(country, xy=(pos_data_x, pos_data_y), xytext=pos_text,
            arrowprops=dict(facecolor='black', width=0.5, shrink=0.1, headwidth=5))
    plt.plot(pos_data_x, pos_data_y, "ro")
plt.xlabel("GDP per capita (USD)")
save_fig('money_happy_scatterplot')
plt.show()sample_data.to_csv(os.path.join("datasets", "lifesat", "lifesat.csv"))sample_data.loc[list(position_text.keys())]
sample_data.plot(kind='scatter', x="GDP per capita", y='Life satisfaction', figsize=(5,3))
plt.xlabel("GDP per capita (USD)")
plt.axis([0, 60000, 0, 10])
X=np.linspace(0, 60000, 1000)
plt.plot(X, 2*X/100000, "r")
plt.text(40000, 2.7, r"$\theta_0 = 0$", fontsize=14, color="r")
plt.text(40000, 1.8, r"$\theta_1 = 2 \times 10^{-5}$", fontsize=14, color="r")
plt.plot(X, 8 - 5*X/100000, "g")
plt.text(5000, 9.1, r"$\theta_0 = 8$", fontsize=14, color="g")
plt.text(5000, 8.2, r"$\theta_1 = -5 \times 10^{-5}$", fontsize=14, color="g")
plt.plot(X, 4 + 5*X/100000, "b")
plt.text(5000, 3.5, r"$\theta_0 = 4$", fontsize=14, color="b")
plt.text(5000, 2.6, r"$\theta_1 = 5 \times 10^{-5}$", fontsize=14, color="b")
save_fig('tweaking_model_params_plot')
plt.show()lin1 = linear_model.LinearRegression()
Xsample = np.c_[sample_data["GDP per capita"]]
ysample = np.c_[sample_data["Life satisfaction"]]
lin1.fit(Xsample, ysample)
t0, t1 = lin1.intercept_[0], lin1.coef_[0][0]
t0, t1sample_data.plot(kind='scatter', x="GDP per capita", y='Life satisfaction', figsize=(5,3))
plt.xlabel("GDP per capita (USD)")
plt.axis([0, 60000, 0, 10])
X=np.linspace(0, 60000, 1000)
plt.plot(X, t0 + t1*X, "b")
plt.text(5000, 3.1, r"$\theta_0 = 4.85$", fontsize=14, color="b")
plt.text(5000, 2.2, r"$\theta_1 = 4.91 \times 10^{-5}$", fontsize=14, color="b")
save_fig('best_fit_model_plot')
xpresso_save_plot("image_1", output_path=MOUNT_PATH,output_folder="jupyter_exp_test/data_prepare")
cyprus_gdp_per_capita = gdp_per_capita.loc["Cyprus"]["GDP per capita"]
print(cyprus_gdp_per_capita)
cyprus_predicted_life_satisfaction = lin1.predict([[cyprus_gdp_per_capita]])[0][0]
cyprus_predicted_life_satisfactionsample_data.plot(kind='scatter', x="GDP per capita", y='Life satisfaction', figsize=(5,3), s=1)
plt.xlabel("GDP per capita (USD)")
X=np.linspace(0, 60000, 1000)
plt.plot(X, t0 + t1*X, "b")
plt.axis([0, 60000, 0, 10])
plt.text(5000, 7.5, r"$\theta_0 = 4.85$", fontsize=14, color="b")
plt.text(5000, 6.6, r"$\theta_1 = 4.91 \times 10^{-5}$", fontsize=14, color="b")
plt.plot([cyprus_gdp_per_capita, cyprus_gdp_per_capita], [0, cyprus_predicted_life_satisfaction], "r--")
plt.text(25000, 5.0, r"Prediction = 5.96", fontsize=14, color="b")
plt.plot(cyprus_gdp_per_capita, cyprus_predicted_life_satisfaction, "ro")
save_fig('cyprus_prediction_plot')
plt.show()sample_data[7:10](5.1+5.7+6.5)/3

try:
    pickle.dump(oecd_bli, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/oecd_bli.pkl", "wb"))
    pickle.dump(gdp_per_capita, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/gdp_per_capita.pkl", "wb"))
    pickle.dump(X, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/X.pkl", "wb"))
    pickle.dump(Y, open("/mnt/nfs/data/jupyter_experiments/projects/jupyter_exp_test/pickles/Y.pkl", "wb"))
except Exception as e:
    print(str(e))
